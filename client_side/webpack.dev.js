const path = require('path')
const webpack = require( 'webpack' );

const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
module.exports = {
    entry: ['@babel/polyfill','./src/index.js'],
    mode: "development",
    output: {
        filename : "bundle.js",
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.ejs$/,
                exclude: [path.resolve( __dirname, 'src/index.ejs')],
                use: {
                    loader: 'ejs-loader',
                    options: {
                        variable: 'data'
                    }
                }
            }, 
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.(css|scss)$/i, 
                use: ['style-loader','css-loader','postcss-loader','sass-loader']
            },
            {
                test: /\.(jpg|jpeg|gif|png)$/i,
                use: [
                {
                    loader:'url-loader',
                        options: {
                            limit: 63000,
                            name: '[name]-[contenthash].[ext]',
                            outputPath:'imagestxt'
                        }
                }]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg|otf)$/,
                use: [{
                    loader: 'file-loader',
                    options:{
                        name:'[name]-[contenthash].[ext]',
                        outputPath:'fonts'

                    }
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [
                            '@babel/plugin-proposal-class-properties',
                            '@babel/plugin-proposal-object-rest-spread',
                            '@babel/plugin-proposal-private-methods'
                        ]
                    }
                }]
            }
        ],
    },// end module

    plugins: [
        new webpack.ProvidePlugin({
            _: "underscore",
            $: 'jquery'

        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.ejs',
            filename: 'index.html'

        })
    ],
    devServer: {
       
        port: 9000,
        contentBase: path.join(__dirname, 'dist'),
        compress:true,
        watchOptions: {
            ignored: /node_modules/
        },
        historyApiFallback : true
    },
    devtool :  'inline-source-map'
}