const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require( 'webpack' );


module.exports = {
    mode:"production",
    entry: ['@babel/polyfill','./src/index.js'],
    output: {
        filename : "[name].bundle.js",
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.ejs$/,
                exclude: [path.resolve( __dirname, 'src/index.ejs')],
                use: {
                    loader: 'ejs-loader',
                    options: {
                        variable: 'data'
                    }
                }
            },
            {
                test: /\.(css|scss)$/i, 
                use: ['style-loader','css-loader','postcss-loader','sass-loader']
            },
            {
                test: /\.(jpg|jpeg|gif|png)$/i,
                use: [
                {
                    loader:'url-loader',
                        options: {
                            limit: 63000,
                            name: '[name]-[contenthash].[ext]',
                            outputPath:'imagestxt'
                        }
                }]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg|otf)$/,
                use: [{
                    loader: 'file-loader',
                    options:{
                        name:'[name]-[contenthash].[ext]',
                        outputPath:'fonts'

                    }
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [
                            '@babel/plugin-proposal-class-properties',
                            '@babel/plugin-proposal-object-rest-spread',
                            '@babel/plugin-proposal-private-methods'
                        ]
                    }
                }]
            }
        ],
    },// end module

    plugins: [
        new webpack.ProvidePlugin({
            _: "underscore",
            $: 'jquery'
        }),

        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.ejs',
            filename: 'index.html'
        }),


    ]

}