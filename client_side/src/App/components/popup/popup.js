import template from './popup.ejs'
import app from '../../app'

export class Popup {

    domComponent 
    popupType = 1 // "connexionAcces"

    handleClick= async ( el )=> {
        if (el.target.id === 'button_register' ) {
            this.popupInit({registration:true})
        } 
        else
        if (el.target.id === 'button_login' ) {
            this.popupInit({registration:false})

        } 
        this.render(app.componentManager.popup_frame)
    }

    popupInit(data){
        const html = template(data)
        const docHtml = app.componentManager.domParser.parseFromString(html, 'text/html')
        this.domComponent = docHtml.getElementById('popup_backend')


    }
    render( frame ) {
        frame.html(this.domComponent)

        /*
            au moment du rendu de popup un tri est fait (pour definir le type de la popup)
                1 = formulaire de type connexion inscription
        */
        if ( this.popupType === 1)
            /*

            LISTENERS SUR POPUP CONNEXION
                1- supression de popup sur click arriere plan 
                2-  ajout de class (active) sur un des formulaires
                    soit connexion  soit inscription
                    lors de click sur switch btn ( afin de masquer le formulaire inutile )
                3- handle registration ( 
                        A -> prend les values des fields 
                            et les envoie au Manager de connexion
                    )
                4- handle login ( Pareil version  )
            */  
            $("#popup_backend").click( function(e) {              //  1
                if(e.target.id === "popup_backend" ) {

                    this.remove()
                } 
            })

            $("#switch_popup_form_button").click( function(e) {     // 2
            
                if($("#popup_login").hasClass("active")) {
                    $("#popup_register").addClass("active")
                    $("#popup_login").removeClass("active")
                } else {
                    $("#popup_register").removeClass("active")
                    $("#popup_login").addClass("active")

                }
    
    
               
            })

            
            $("#register_button_send").click(  async (e) => {         //  3
                
                const pseudo = $("#register_input_pseudo").val()      //  3 - A
                const email = $("#register_input_email").val()
                const password = $("#register_input_password").val()

                await app.userManager.handleConnexion(password, email, pseudo)

                
            } )
 
            $("#login_button_send").click(  async (e) => {            //  4

                const identifiant = $("#login_input_pseudo").val()
                const password = $("#login_input_password").val()

                await app.userManager.handleConnexion(password, identifiant)
                
            } )

    }
    remove(frame){
        console.log("suppression du rendu de popup ");
        frame.html('')

    }

    handleNotifications(notifications){
        console.log(notifications);
    }

}