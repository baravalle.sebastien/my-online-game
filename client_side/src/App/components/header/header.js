import template from './header.ejs' 
import app from '../../app'

export class Header {
    
    domComponent 


    constructor(user) {

        const html = template(user)
        const docHtml = app.componentManager.domParser.parseFromString(html, 'text/html')
        this.domComponent = docHtml.getElementById('nav_id')
    
    }

    render( frame ){
        console.log("nouveau rendu de header : ");
        frame.html(this.domComponent)
        this.classSetter(app.user)
        this.setListeners(app.user)
        

    }
    classSetter(user){
        // class pour Home Page
        if( !user.isConnected && !user.isInGame){
            $("#logo_frame").addClass( "home" )
            $("#header_frame").addClass( "home" )
            $("#header_border").addClass( "home" )



        } else {
            $("#logo_frame").removeClass( "home" )
            $("#header_frame").removeClass( "home" )
            $("#header_border").removeClass( "home" )

        }

    }

    setListeners(user ) {

        if( user.isInGame ){

            $('#button_quit_game').click((evt)=> {
                app.gameManager.quitGame()
            })


            $('#button_logout').click((evt)=> {
                app.userManager.logOut()
            })

        }
        else if ( user.isConnected ) {
            // $('#button_join_game').click( this.formSend.bind(this))

            $('#button_logout').click((evt)=> {
                app.userManager.logOut()
            })


        } else 
        {      

            $('#button_login').click((evt)=>{
                app.componentManager.popupComponent.handleClick(evt)

            })
            $('#button_register').click((evt)=>{
                app.componentManager.popupComponent.handleClick(evt)

            })


        }
    }
}