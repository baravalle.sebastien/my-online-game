import template from './menu.ejs' 
import app from '../../app'

export class Menu {
    
    domComponent 


    constructor(user) {

        const html = template(user)
        const docHtml = app.componentManager.domParser.parseFromString(html, 'text/html')
        this.domComponent = docHtml.getElementById('menu_id')
    
    }

    render( frame ){
        frame.append(this.domComponent)
        this.classSetter(app.user)
        this.setListeners(app.user)


    }
    classSetter(user){

    }
    remove(){
        this.domComponent.remove()

    }
    setListeners(user ) {

        if ( user.isConnected ) {
            // $('#button_join_game').click( this.formSend.bind(this))
            $('#button_create_a_game').click((evt)=> {
                app.gameManager.createGame()
            })

            $('#button_join_game').click((evt)=> {
                app.gameManager.joinGame()
            })



        } 
    }
}