import {Game } from "../models/game"
import app from '../app'
import {Header} from '../components/header/header'

export class GameManager {
    get URLroot() {return 'http://127.0.0.1:3000'}

    get storageName() {return 'id_last_game'}
    
    

    constructor(){

    }
    async createGame(){
        const url = this.URLroot+'/game/create_new_game'
        const headers = new Headers()
        headers.append('Accept', 'application/json')
        headers.append('Authorization', 'Bearer '+app.user.token)
        const request = {
            method: 'POST',
            headers,
            body: {}
        }

        try {
            const game = await fetch(url, request)
                .then( res => res.json())

            app.startGame(game)



            
        } catch(e) {

        }

    }

    async joinGame(){
        const url = this.URLroot+'/game/join_game'
        const headers = new Headers()
        headers.append('Accept', 'application/json')
        headers.append('Authorization', 'Bearer '+app.user.token)
        const request = {
            method: 'POST',
            headers,
            body: {}
        }
        try {
            const game = await fetch(url, request)
                .then( res => res.json())

            app.startGame(game)


            
        } catch(e) {
            console.log(e);
        }

    }

    async sendMoove(player) {
        const url = this.URLroot+'/game/send_moove'
        const headers = new Headers()
        headers.append('Accept', 'application/json')
        headers.append('Content-Type', 'application/json')
        headers.append('Authorization', 'Bearer '+app.user.token)

        const request = {
            method: 'POST',
            headers,
            body: JSON.stringify({
            
               player,
               id: app.game._id,
               userId : app.user.id
            })
        }
        try {
            const modificationIsSaved = await fetch(url, request)
                .then( res => res.json())
            
        } catch(e) {
            console.log(" erreur lors de l'envoie de nouvelle positions ", e);
        }

        return true

    }

    async watchForChanges(i) {

        

        const url = this.URLroot+'/game/watch'
        const headers = new Headers()
        headers.append('Accept', 'application/json')
        headers.append('Content-Type', 'application/json')

        const request = {
            method: 'POST',
            headers,
            body: JSON.stringify({
                step: i,
                id :app.game._id,
                currentIndex : app.game.map.currentPlayer.index
            })
        }


        const requestResponse = await fetch(url, request)
        const data = requestResponse.json()
        return data

    }

    async watchForNewPlayers() {
            const url = this.URLroot+'/game/watch_for_players'
            const headers = new Headers()
            headers.append('Accept', 'application/json')
            headers.append('Content-Type', 'application/json')
    
            const request = {
                method: 'POST',
                headers,
                body: JSON.stringify({
                    id :app.game._id,
                    currentIndex : app.game.map.currentPlayer.index
                })
            }
    
    
            const requestResponse = await fetch(url, request)
            console.log(" changement de joueur : ");
            const data = requestResponse.json()
            return data
    

    }
    async manageLogWriter(notFirstLogWriter= undefined) {
        const url = this.URLroot+'/game/log_writer'
        const headers = new Headers()
        headers.append('Accept', 'application/json')
        headers.append('Content-Type', 'application/json')
        console.log('ID Du Joueur: ', app.game.map.currentPlayer.index)
        const request = {
            method: 'POST',
            headers,
            body: JSON.stringify({
                notFirstLogWriter,
                gameId :app.game._id,
                currentIndex : app.game.map.currentPlayer.index,
                userId : app.user.id 
            })
        }

        
        const requestResponse = await fetch(url, request)

        return true


    }

    async getLog(){
        const url = this.URLroot+'/game/get_log'
        const headers = new Headers()
        headers.append('Accept', 'application/json')
        headers.append('Content-Type', 'application/json')

        const request = {
    
            method: 'POST',
            headers,
            body: JSON.stringify({
                gameId :app.game._id,
                userId : app.user.id ,
                
            })
        }


        const requestResponse = await fetch(url, request)
        const data = requestResponse.json()
        return data


    }

    async quitGame(){
        const url = this.URLroot+'/game/quit_game'
        const headers = new Headers()
        headers.append('Accept', 'application/json')
        headers.append('Authorization', 'Bearer '+app.user.token)
        const request = {
            method: 'POST',
            headers,
            body: {}
        }

        
        const response = await fetch(url, request)
                        .then( res => res.json())
            
        document.location.reload()


    }
    
}