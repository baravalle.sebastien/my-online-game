import { User } from "../models/user";
import app from '../app'
import { Game } from "../models/game";

export class UserManager {

    get URLroot() {return 'http://127.0.0.1:3000'}

    get storageName() {return 'my_game_local_storage'}

    /*
        I: user init()
            1- cherche si un Token est déja présent
                -> retourne un user vide
            ou 
            2- connecte l'utilisateur            
    */
    async userInit(){
        console.log('initialisation de User ');
        const userHasToken = await this.findIfUserHasToken()          // 1
        if (  !userHasToken ) {
            console.log("aucun token dans le local storage ");

            return new User(undefined)

        } else 
        {
            console.log("token trouvé dans le local storage ");

            return await this.connectUserByToken(userHasToken)  //  2

        }        
    }
    findIfUserHasToken() {
        try {
            let Data = localStorage.getItem(this.storageName)
            Data = JSON.parse(Data)
            if( Data ) return Data

        } catch (e) {
            return false 
        }
        return false 
        
    }
    async connectUserByToken(token){
        console.log("tentative de connexion au serveur avec le token");
        const url = this.URLroot+'/user/me'
        
        const headers = new Headers()
        headers.append('Authorization', 'Bearer '+token)
        const request = {
            method: 'GET',
            headers
        }
        try {

            const user = await fetch( url , request)
                .then( res => {
                    if (res.status === 200 ) return res.json()
                    return null

                })
                .then( data => {

                    if ( data && data._id ){
                        console.log(" connexion réussie, user receptionné ");
                        return new User (data,token)
                    } 

                    console.log(" erreur, entitée receptionnée est non conforme ");
                    localStorage.clear()
                    return new User (undefined)

                })
            return user

        } catch (e) {

            console.log(" erreur lors de la connexion: ", e );
            return new User (e)
            
        }

    }



    
    /*

        II. HANDLE CONNEXION :

            handle Connexion est appellée* lors de l'envoie
            d'un formulaire d'inscription (registration) ou de
            de connexion (login)
            
            1- teste la validité des inputs text ( non null, emailType, etc.. )
                .a-> si invalide retourne notification
            2- determine si il faut faire une requete de REGISTRATION ou de LOGIN
                -> appelle la methode concernée
            3- selon la reponse de la requete:
                .a-> appelle une notification d'echec
                .b-> cree un nouveau rendu adapté a l'user connecté
            *(elle est appellé par l'event sur les boutons send formulaire de popupComponent)
            

    */ 
    async handleConnexion(password= undefined, identifiantOrEmail = undefined, pseudo= undefined){

        const notifications = this.fieldVerify(password,identifiantOrEmail,pseudo)      //  1
        if (notifications.length > 0) return app.componentManager.handleNotifications(notifications)     //  1.a    

        const connexionStatusAndNotif = (password && identifiantOrEmail && pseudo  ) ?          //  2
            await this.registerUser(password,identifiantOrEmail,pseudo)
            :
            await this.logUser(password,identifiantOrEmail)


        if(connexionStatusAndNotif.status === false ) 
        return  app.componentManager.handleNotifications(connexionStatusAndNotif)
        
        app.startMenu(connexionStatusAndNotif) // connexionResult est une notification transmise au render 
        

    }
    /**
        II - 1 vérifie les fields
        
        elle determine si c'est un form :
            1- d'inscription:                   (et vérifie)
                -password 
                -email 
                -pseudo
            2- connexion:                       (et vérifie)
                -password 
        retourne les notification de vérification
     */
     fieldVerify( password, identifiantOrEmail, pseudo = undefined ) {
        const notifications = {
            status: false,
            messages: []
        }
        if(!password) notifications.messages.push("veuillez renseigner votre mot de passe")
        if(!identifiantOrEmail) notifications.messages.push("veuillez renseigner tout les champs")

        if (password.length <=  6 && password.length >= 250 ) notifications.messages.push( "veuillez mettre un mot de passe de plus de six lettre")
        
        if ( !pseudo) {                 // alors form de connexion
            if (identifiantOrEmail.length <= 3 ) notifications.messages.push("veuillez renseigner votre identifiant ou votre email")
        } else if ( pseudo && password && identifiantOrEmail) {
            if(! identifiantOrEmail.match(/^[a-z0-9._-]{1,20}@[a-z0-9._-]{2,12}\.[a-z]{2,7}$/) ) notifications.messages.push("cet email est invalide")
            if(! pseudo.length > 3 )  notifications.messages.push(" veuillez renseigner un identifiant de plus de 3 lettres")
            
        }
        return notifications

    }

    /*
        II - 2 - a 
        methode appellée dans handle connexion 
        afin d'enregistre un nouvel utilisateur
        retourne une notification et un booléen disant si 
        l'utilisateur a bien été crée
        elle instancie un nouvel user dans App
    */
    async registerUser(password, email, pseudo ) {
        const url = this.URLroot+'/user/register'
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')


        const request = {
            method: 'POST',
            headers,
            body: JSON.stringify({
               login : pseudo,
               email,
               password 
            })
        }

        try {
            const user = await fetch(url, request)
                .then( res => res.json())
                

            if ( !user ) return { status: false, messages:["identifiants déja existant"]}
            
            app.user = new User(user.user, user.token )
            this.saveToStorage(user.token)
            return { status: true, messages:[" Bienvenue "+user.user.login+" vous êtes connecté."]}


        } catch (e) {
            console.log("erreur : ",e);
            return { status: false, messages:["identifiants déja existant"]}

        }



        return user
    }
    /*
        II - 2 - a 
        methode appellée dans handle connexion 
        afin de Connecter un utilisateur 
        retourne une notification est un booléen disant si 
        l'utilisateur a bien été crée
        elle instancie un nouvel user dans App
    */
    async logUser(password, identifiant ) {

        const url = this.URLroot+'/user/login'
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')

        const request = {
            method: 'POST',
            headers,
            body: JSON.stringify({
               password,
               identifiant
            })
        }

        try {
 
            const user = await fetch(url, request)
                .then( res => res.json())
                .then( data => data)
 
            if ( !user ) return { status: false, messages:["les informations renseignés sont invalides"]}
            
            app.user = new User(user, user.token)

            this.saveToStorage(user.token)

            return { status: true, messages:[" Bienvenue "+user.user.login+" vous êtes connecté."]}
 
        } catch (e) {
            return { status: false, messages:["Une erreur s'est produite"]}
        }




    }
    saveToStorage(item) {
        localStorage.setItem(this.storageName, JSON.stringify(item))

    }

    async logOut() {
        console.log(" debut de déconnexion ");
        const url = this.URLroot+'/user/log_out'
        const headers = new Headers()
        headers.append('Authorization', 'Bearer '+app.user.token)
        const request = {
            method: 'POST',
            headers,
            body: {}
        }
        try {
            const resultat = await fetch(url, request)
                .then( res => res.json())
            
            app.user = new User(undefined)

            console.log(' resultat de la requete ', resultat);
            localStorage.clear()
            if ( app.game && app.game.context ) {
                console.log(" context nettoyé");
                app.game.context.clearRect(0,0, app.componentManager.canvas.width, app.componentManager.canvas.height)
            }
            app.game = {}
            // delete Token 
            app.componentManager.renderHome(app.user)
            document.location.reload()
            
        } catch(e) {
            console.log('erreur lors de déconnexion : ',e);
        }


        
    }

}