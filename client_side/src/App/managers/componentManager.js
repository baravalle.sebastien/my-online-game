import app from '../app'
import {Header} from '../components/header/header'
import {Popup} from '../components/popup/popup'
import {Menu} from '../components/menu/menu'
export class ComponentManager {
    nav_frame; 
    popup_frame 
    
    // component 
    headerComponent; popupComponent; canvas
    menuComponent; menu_frame

    // JS properties
    domParser; 






    constructor(){
        this.domParser = new DOMParser()
        this.nav_frame = $('#nav_frame')
        this.popup_frame = $('#popup_frame')
        this.canvas =  document.querySelector('#canvas');
        this.popupComponent = new Popup()
        this.menu_frame = $('#canvas_style_element')

    }


    handleNotifications(notifications){
        // 
        console.log("notifications: ", notifications);
    }
  

    setSpecifiedHeader(user){
        this.headerComponent = new Header(user)
        this.headerComponent.render(this.nav_frame)

    }

    renderForGame(user){
        this.setSpecifiedHeader(user)
        this.canvas.className +=" active"
        $('#canvas_style_element').addClass( "active" );
        $('#canvas_frame').addClass( "inGame" );
        this.menuComponent.remove()
        $('#canvas_frame').removeClass('menu')

    }
    renderMenu(user, notification= undefined){
        this.setSpecifiedHeader(user)
        $('#main_frame').removeClass('home')
        this.popupComponent.remove(this.popup_frame)
        this.menuComponent = new Menu(user)
        this.menuComponent.render(this.menu_frame)
        $('#canvas_frame').addClass('menu')

    }
    renderHome(user ) {
        if(user.isConnected) return this.renderMenu(user)
        this.setSpecifiedHeader(user)
        $('#main_frame').addClass('home')

    }



}