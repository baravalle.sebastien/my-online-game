import {UserManager} from './managers/userManager'
import {GameManager } from './managers/gameManager'
import { ComponentManager } from './managers/componentManager';
import {Game} from './models/game'
class App {

    //MANAGER and model :
    componentManager
    userManager; user
    gameManager; game
    
    constructor(){
        this.componentManager = new ComponentManager()
        this.userManager = new UserManager()
        this.gameManager = new GameManager()
        

    }



    async start() {

        this.user = await this.userManager.userInit()
        this.componentManager.renderHome(this.user)        
        
    }

    /*
        appelé dans user manager une fois que l'utilisateur est connecté
        un nouvel user a été instancié dans log User ou Register user 

    */
    startMenu(notification){
        this.componentManager.renderMenu(app.user, notification )

    }

    /*
        appellé dans game manager suite  a une connexion a une partie
    */
    startGame(game){
        this.user.isInGame = true 
        this.componentManager.renderForGame( this.user)
        this.game = new Game (game)
        this.game.start()


    }


}

const app = new App()

export default app