import {Map} from './gameSubClasses/map'
import app from './../app'
export class Game {
    _id 
    map 

    context
    imgData

    moveSaved = true

    constructor(game){
        this._id = game._id
        this.map = new Map(game.players)
        
        
    }

    /**
                    players
        Game Map Tileset|
            |   |   |   |
            v   v   v   v
        étapes de fonctionnement Game
            1- instancie new Map("map")
                |--> pour chaque Players instancie player ( id coordonée)
                        |-->stoque cooredonée sur map, direction ,pseudo 
                |-->recuperer tiles data 
                |--> 2- instancie new Tileset
                    |-->stoque son image et sa largeur (en case de 32px )
                |-->affecte dispositon des tuilles avec Data
                |-->affecte l'image des sprites 
            
            2- Start() 
            |--> instancie context
            |--> recuperer canvas, 
            |-->l'egaliser avec les largeur et hauteur de l'entity map 
                |--> (map.getHeight() getWidth() )
            |--> appelle la methode draw map draw()
                |--> pour chaque ligne du tableau 
                    elle extrait chaque case, rend sa position en x
                    et en y , la valeur de tile, et appelle tileset.drawTile
                    |-->recupere la position de la tile sur l'image tileset
                    |--> l'ajoute au context a la bone position
                |--> clonne la map réalisée
            
            3- Boucle de dessin du context 
            |--> clone l'image map sans personnage 
            |--> appelle map stream 1e fois pour dessiner les personnages imobiles
                |--> pour chaque players--> player.drawPlayer()
                        |--> cf player.drawPlayer()
            
            4- listeners de keyEvents
            |--> is a valid key prend le key event 
            |--> si key == direction Player.animation()
                        |--> animationDéplacement
                        |--> si pas déja un déplacement, si il est valide (pas sur la boue )
                        |--> modifie les coordonnées de Player () déclache état animation = 1
                        |--> retourne true si mouvement enregistrée
            |--> si valide Mouvement:
            |--> promesse recursive animeMouvement() --> attend 25 ms --> déclange 
                |--> map Stream ()
                |--> affiche l'image de carte sans players
                |--> pour chaque players les dessine a leurs positions:
                        |--> PlayerDrawPlayer() 
                |--> si état animation actif (ex: 1) -> affiche l'image adéquate et incrément l'état
            |--> si état animation fini --> resout animeMouvement
            |--> sinon la rappelle.

            5-  envoie des coordonnes lors de mouvement 
            
            6-  Gere les mouvement des autres joueurs (promesse récursive infinie )  
                |--> get public renvoi les datas nécéssaire a prendre en compte un nouveau déplacement
            |--> passe les données au gameManager watchForChanges  
     */


    async start(){

        this.context = app.componentManager.canvas.getContext('2d')
        await this.map.tileset.tilesetInit()
        app.componentManager.canvas.width = this.map.getWidth()* 32
        app.componentManager.canvas.height = this.map.getHeight()* 32
        
        await this.map.drawMap(this.context);

        await this.map.spriteImgInit()

        this.context.putImageData(this.imgData,0, 0)

        this.map.mapStream(this.context)

        
        

        this.setKeyListener()
    
        
        this.initLogWriter()
        this.markNewPlayers()
        this.dispatchLastsMoves()

    }

    async initLogWriter(i = undefined){
        await  app.gameManager.manageLogWriter(i)
        this.initLogWriter(1)

    }

    async dispatchLastsMoves( ){
        const logs = await app.gameManager.getLog()
        console.log('RECUE', logs)

        
        logs.forEach((log  ) =>  {
            
                const player = {
                    x:log.x,
                    y:log.y,
                    direction:log.direction,
                }
                this.map.serverStream(player, log.index )              
            

        });


        // traitement
        this.dispatchLastsMoves()
    }


    async markNewPlayers(){
                
        const players = await app.gameManager.watchForNewPlayers()
        console.log(" CHANGEMENT DE JOUEUR : ", players)
        if(! players) this.markNewPlayers()

        else if (players.data.operation === 2 )
        {

            // un joueur a été ajouté 
            
            this.map.addPlayer(players.data.data)
            this.context.putImageData(this.imgData,0, 0)
            
            this.map.mapStream(this.context)
            return this.markNewPlayers()

        } 
        else if (players.data.operation === 3 )
        {
            // un joueur a été suprimmé
            console.log("joueur suprimé ");
            this.map.erasePlayers(players.data.data)
            this.context.putImageData(this.imgData,0, 0)

            this.map.mapStream(this.context)
            return this.markNewPlayers()
        } 



    }


    async setKeyListener(){

        var DUREE_DEPLACEMENT = 9;

        var DIRECTION = {
            "BAS"    : 0,
            "GAUCHE" : 1,
            "DROITE" : 2,
            "HAUT"   : 3,
            "HG": 4,
            "HD":5,
            "BD":6,
            "BG":7
        }

        window.onkeydown= (e)=>{
            // set moveData met en x et y les prochaine case 
            if(this.map.currentPlayer.etatAnimation > -1 ) return false;
            const key = e.keyCode

            const isValidKey = (key ) => {

                
                switch(key) {
    
                    case 38 : 
                        
                        return this.map.currentPlayer.setMoveData(DIRECTION.HAUT, this.map.TilsDispositonArray);

                        break;
                    case 40 : 
                        return this.map.currentPlayer.setMoveData(DIRECTION.BAS, this.map.TilsDispositonArray);
        
                        break;
                    case 37 : 
                        return this.map.currentPlayer.setMoveData(DIRECTION.GAUCHE, this.map.TilsDispositonArray);
                        break;
                    case 39 :
                        return this.map.currentPlayer.setMoveData(DIRECTION.DROITE, this.map.TilsDispositonArray);
                        break;
         
                    case 36 : 
                        return this.map.currentPlayer.setMoveData(DIRECTION.HG, this.map.TilsDispositonArray);
                        break;
                    case 33 : 
                        return this.map.currentPlayer.setMoveData(DIRECTION.HD, this.map.TilsDispositonArray);
                        break;
                    case 34 :
                        return this.map.currentPlayer.setMoveData(DIRECTION.BD, this.map.TilsDispositonArray);
                        break;   
                    case 35 : 
                        return this.map.currentPlayer.setMoveData(DIRECTION.BG, this.map.TilsDispositonArray);
                        break;
                    default : 
                       
                        return false;
                }
            }

            const valid = isValidKey(key)
            if ( valid === true ) {
                
    
                const animeMouvement = ()=> {
                    return new Promise ( async (resolve, reject ) => {
                        setTimeout( ()=> {
                            this.context.putImageData(this.imgData,0, 0)
                            
                            this.map.mapStream(this.context);
                            
                            if( this.map.currentPlayer.etatAnimation >= 0 ){
                                animeMouvement()
                                
                            } else {
                                
                                resolve()
                            }
                            
                        }, 20 )
                        
                    })
                }
                // etat de joueur aprés l'animation ( fin de mouvement ) est envoyé 
                const playerPublic = {
                    x: this.map.currentPlayer.x,
                    y: this.map.currentPlayer.y,
                    direction: this.map.currentPlayer.direction,
                    index: this.map.currentPlayer.index,
                }

                animeMouvement()
                this.handleMoove(playerPublic)

            }

        }
            

    }

    async handleMoove(player) {
        this.moveSaved = false
        const mooveIsSaved = await app.gameManager.sendMoove(player)
        if ( mooveIsSaved ) this.moveSaved = true
        else this.moveSaved = true

    }
    
    async animeMouvementOtherPlayers(player) {
        return new Promise ( async (resolve, reject ) => {
            setTimeout( ()=> {
                this.context.putImageData(this.imgData,0, 0)
                
                this.map.mapStream(this.context);
                
                if( player.etatAnimation >= 0 ){
                    console.log(" état animation: ",player.etatAnimation);

                    this.animeMouvementOtherPlayers(player)
                    
                } else {
                    
                    resolve()
                }
                
            }, 20 )
            
        })
    }


}