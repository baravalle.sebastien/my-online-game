export class User {
    // proprietée du joueur
    isConnected = false
    token 
    id; email
    login


    constructor (model = undefined, token= undefined )  {

        // cas ou user est crée d'apres retour valide token 
        if(model && model._id){

            this.isConnected = true
            if ( model._id ) this.id = model._id
            if ( model.email ) this.email = model.email
            if ( model.login ) this.login = model.login
        }  
        if(token ) {
            this.token = token
        } else {

        }
        if(model && model.user) {
            this.isConnected = true
            if ( model.user._id ) this.id = model.user._id
            if ( model.user.email ) this.email = model.user.email
            if ( model.user.login ) this.login = model.user.login
            if ( model.token ) this.token = model.token

        }
        
        if( this.token ){
            console.log("User connexion réussie (valide token )", this);

        } else {
            console.log("User non connecté " , this);

        }
    }

}