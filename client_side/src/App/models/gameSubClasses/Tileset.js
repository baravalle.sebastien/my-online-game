import image from  './tileset_n1.png'

export class Tileset{

    image 
    /*
        tileset est mon set de tuile 
        des carés mis cote a cotes, 
        Tileset les découpe et les assemble selon le 
        tableau que je lui passerai
    */
    constructor () {

        // image javascript destiné a être manipulée
    }

    tilesetInit() {
        return new Promise(next=>{
            this.image = new Image()
            this.image.src = image
            this.image.onload = ()=> {
                this.largeur = this.image.width /32
                next()
            }
    
        })
    }

    drawTile(tile, context, xD, yD){
        let xSource = (tile % this.largeur)
        // exemple 30 % 9 = 3 (30/9 -> 27 reste 3)  -> la case 30 est sur la troisieme colone
        if(xSource== 0 ) xSource= this.largeur;
        xSource = (xSource-1)*32

        const ySource = ((Math.ceil(tile / this.largeur))-1)*32
        // exemple ceil(30 / 9) -> 3.3 -> 3  la case 30 est sur la 3eme ligne
        

        context.drawImage(this.image, xSource, ySource,32,32, xD, yD,32,32);

    }
}