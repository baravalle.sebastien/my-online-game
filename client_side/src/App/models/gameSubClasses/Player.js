var DIRECTION = {
	"BAS"    : 0,
	"GAUCHE" : 1,
	"DROITE" : 2,
    "HAUT"   : 3,
    "HG": 4,
    "HD":5,
    "BD":6,
    "BG":7
}
var DUREE_ANIMATION = 1;
var DUREE_DEPLACEMENT = 9;


export class Player {
    x
    y
    direction
    mvt
    etatAnimation = -1
    pseudo
    index
    waitingMoves = 0

    
    constructor(player, index){
        this.x = player.x
        this.y = player.y
        this.direction = player.direction
        this.pseudo = player.pseudo
        this.mvt = false
        this.index = index

    }

    drawPlayer(context, imageSprite) {
        
        let frame = 0 
        let decalageX = 0 
        let decalageY = 0 
        /**
            determiner si une animation est en cours: 
            lorsqu'une animation est en cours, 
            etatAnimation s'incremente achaque appelle de draw player
            jusqu'a dépasser la durée en tour de boucle d'un déplacement 
            alors l'animation est finie, on peut l'arreter

            si l'état d'anim et superieur a 0 il faut 
            la continuer

         */
        if (this.etatAnimation >= DUREE_DEPLACEMENT ) {
            this.etatAnimation = -1

        } else if ( this.etatAnimation >= 0 )
        {
            /**
                l'image qui contient les sprites aligne
                chaque image propre a un état d'animation
                selon que l'état d'animation soit au début ou a la fin
                frame respresente l'image adéquate
                ex: etatAnim = 0 
                    floor(0 / 2 ) -> 0
                    floor(1 / 2 ) -> 0
                    floor(2 / 2 ) -> 1
                    floor(3 / 2 ) -> 1
                    4 -> 2 
                    5 -> 2 
                    6 -> 3   
                    7 -> 3 
                    le deuxieme traitement repete l'animation ( retour a premiere image )
                    8 -> 4 -> ( 0 )
                    9 -> 4 -> ( 0 )
                    10-> 5 -> ( 1 )

             */
            frame = Math.floor(this.etatAnimation / DUREE_ANIMATION);
            if ( frame > 3 ) frame %= 4             // deuxieme traitement 

            // ex : 32 - ( 32 * (9 / 2 ) -> (32 - 144) 
            var pixelsAParcourir = 32 - (32 * (this.etatAnimation / DUREE_DEPLACEMENT));
            

            if(this.direction == DIRECTION.HAUT) {
                decalageY = pixelsAParcourir;
            } else if(this.direction == DIRECTION.BAS) {
                decalageY = -pixelsAParcourir;
            } else if(this.direction == DIRECTION.GAUCHE) {
                decalageX = pixelsAParcourir;
            } else if(this.direction == DIRECTION.DROITE) {
                decalageX = -pixelsAParcourir;
            }else if(this.direction == DIRECTION.HD) {
                decalageY = pixelsAParcourir;
                decalageX = -pixelsAParcourir;
    
            } else if(this.direction == DIRECTION.HG) {
                decalageY = pixelsAParcourir;
                decalageX = pixelsAParcourir;
    
            } else if(this.direction == DIRECTION.BD) {
                decalageY = -pixelsAParcourir;
                decalageX = -pixelsAParcourir;
    
            } else if(this.direction == DIRECTION.BG) {
                decalageY = -pixelsAParcourir;
                decalageX = pixelsAParcourir;
            }

            this.etatAnimation++;
        }

        let imagePosition = this.direction;
        if(imagePosition>3){
    
            imagePosition = imagePosition == 4 ? 1 : imagePosition;
            imagePosition = imagePosition == 5 ? 2 : imagePosition;
            imagePosition = imagePosition == 6 ? 2: imagePosition;
            imagePosition = imagePosition == 7 ? 1: imagePosition;
        
        }

        context.drawImage(
        
            imageSprite, 
            32 * frame, imagePosition * 32, // Point d'origine du rectangle source à prendre dans notre image
            32, 32, // Taille du rectangle source (c'est la taille du personnage)
            (this.x * 32) - (32 / 2) + 16+decalageX, (this.y * 32) - 32 + 24+decalageY, // Point de destination (dépend de la taille du personnage)
            32, 32 // Taille du rectangle destination (c'est la taille du personnage)
        );
    
    
    }

    setMoveData(direction, terrain ) {

        this.direction = direction;

        const prochaineCase = this.getCoordonneesAdjacentes(direction);

        if(terrain[prochaineCase.y][prochaineCase.x] == 30) {
            return false;
        }
    	this.x = prochaineCase.x;
	    this.y = prochaineCase.y;
        this.etatAnimation = 1;	
	    return true;


    }

    async getPresentState() {
        if(this.etatAnimation >= 0) {
            return  this.etatAnimation;
        }
	    return 9;


    }
    setNewDirectionPoint(direction){
        this.direction = direction;
        const prochaineCase = this.getCoordonneesAdjacentes(direction);

    	this.x = prochaineCase.x;
        this.y = prochaineCase.y;
        this.etatAnimation = 1;	
    }

    getFuturePosition(){
        
    }

    setPlayerValue(player){
        this.x = player.x
        this.y = player.y

    }

    getCoordonneesAdjacentes(direction) {
        const coord = {'x' : this.x, 'y' : this.y};
        switch(direction) {
            case DIRECTION.BAS : 
                coord.y++;
                break;
            case DIRECTION.GAUCHE : 
                coord.x--;
                break;
            case DIRECTION.DROITE : 
                coord.x++;
                break;
            case DIRECTION.HAUT : 
                coord.y--;
                break;
            case DIRECTION.HG : 
                coord.y--;
                coord.x--;
                break;
            case DIRECTION.HD :
                coord.x++; 
                coord.y--;
                break;
            case DIRECTION.BD : 
                coord.x++;
                coord.y ++;
                break;
            case DIRECTION.BG : 
                coord.x--;
                coord.y++;
                break;
        }
        return coord;
    
    }


}