import {Tileset} from './Tileset'
import {mapData} from './mapData'
import spriteImg from './sprite.png'
import {Player} from './Player'
import app from './../../app'

export class Map{
    
    // est un array des joueurs présent sur la carte
    // de leurs positions 
    players= []
    currentPlayer  


    tileset // image brute des tuiles
    TilsDispositonArray // données brutes de leurs redisposition
    sprite // image brute des personage

    constructor ( players ){

        players.forEach((player, index ) => {
            if (player.pseudo === app.user.login) {
                this.currentPlayer = new Player(player, index )
                this.players.push(0)

            } else {

                this.players.push( new Player(player, index ))
            }
        });

        //recupération du json Carte (tableau de répartition des tuiles)

        this.tileset = new Tileset()

        this.TilsDispositonArray = mapData.terrain
    }

    async spriteImgInit() {
        return new Promise(next=>{

            this.sprite = new Image()

            this.sprite.src = spriteImg
    

            this.sprite.onload = ()=> {
                next()
            }
    
        })
    }
    
    erasePlayers(players){
        this.players = []
        players.forEach((player, index ) => {
            if (player.pseudo === app.user.login) {
                
                this.players.push(0)
                
            } else {
                
                this.players.push( new Player(player, index ))
            }
        });
        console.log(" player erased ");
        
    }


    getWidth(){
        return this.TilsDispositonArray[0].length
    }

    getHeight(){
        return this.TilsDispositonArray.length
    }

    
    getPublicPlayers() {
        const publicPlayers = []
        this.players.forEach( (player) =>  {
            const publicPlayer = {
                x: player.x,
                y: player.y,
                direction: player.direction,
                index: player.index
            }
            publicPlayers.push(publicPlayer)
        });
        return publicPlayers
    }
    async drawMap( context ){
        let width = 0
        const height = this.TilsDispositonArray.length
        this.TilsDispositonArray.forEach( (range, i ) => {
            
            width = range.length
            const y = i*32
            
            range.forEach( (tile, j) => {
            
                this.tileset.drawTile(tile, context, (j*32), y )

            });
        });
        
        const img = context.getImageData(0,0, width*32 , height*32 )
        
        app.game.imgData = img

    }

    mapStream( context ){
        
        this.currentPlayer.drawPlayer(context, this.sprite )

        this.players.forEach(player => {
            if(player !== 0){
                player.drawPlayer(context, this.sprite)

            }
        });
        
    }

    /*
        
        un mouvement non pris en compte est remonté a server stream
        il trouve le joueur concerné, l'anim dans direction
        si les position x et y sont pas les mêm il les remet bien


        SERVER STREAM reçoit la position du joueur aprés son mouvement 
            -> get present state indique si un mouvement est en cours 
            -> setNewDirection (avec la direction reçue ) calcule la position a venir

    */
    async serverStream(player, index  ){

        const renderInterval = 20
        const nbOfIntervalForMove = 9
        const animationState = await this.players[ index].getPresentState()


        if( this.players[index].waitingMoves !==0 ||(animationState != 9 )) { 

              // si des mouvement sont non fini ou en attente (les déclancher quand libre )
              const timeBeforeNewMove = ((nbOfIntervalForMove - animationState) * renderInterval )+(nbOfIntervalForMove * renderInterval *this.players[index].waitingMoves )
              setTimeout(() => {


                this.players[ index].setNewDirectionPoint(player.direction)
                app.game.animeMouvementOtherPlayers(this.players[ index])
                this.players[index].waitingMoves --
    
                    
                    
            }, timeBeforeNewMove )
            this.players[index].waitingMoves ++ 


        } else {
            
            this.players[ index].setNewDirectionPoint(player.direction)

            app.game.animeMouvementOtherPlayers(this.players[ index])

            if(
                this.players[ index].x !== player.x 
                || this.players[ index].y !== player.y  
                ) this.players[ index].setPlayerValue(player) 


        }
        
            
    }
        
    addPlayer(player  ){
    
    // un nouveau joueeur est ajouté, on lui passe son index
    // pour faciliter les opération auprès de l'api : 

    this.players.push( new Player(player,this.players.length ))

          
       
    }

}