const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const validator = require('validator')
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
    login: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        minlength: 4,

    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true,
        validate( value ) {
            if ( !validator.isEmail( value ) ) {    throw new Error ( 'Email is invalid'); }
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 3,
    },
    tokens: [{
        token: {
            type: String,
            required: true

        }
    }],
}, {
    timestamps: true      
    }
)

const Game = require('./game')

userSchema.pre('save', async function(next , summary ) {

    if (this.isModified('password')) this.password = await bcrypt.hash(this.password, 8 )
    
    if(this.isModified('tokens') && summary && !this.tokens[0]) {

        console.log("lors de modification de token (LOGOUT) ");
        
        const result = await Game.findOneAndUpdate({
            "players.player": this._id
        },{
            $pull : {
                players : {
                   player : this._id 
                }
            }
        },{ returnOriginal: false } )

        console.log("player sortie de partie : ", result);
    }
    next()
})
    
userSchema.pre('remove', async function (next ){

    const tasks = await Task.deleteMany({owner : this._id})

    next()
})

userSchema.methods.generateAuthToken= async function(){
    try {
        const token = jwt.sign({_id: this._id.toString()}, 'wxb',)
        this.tokens =  this.tokens.concat({ token })
        await this.save()
        return token
    } catch(e){
        console.log(e);
    }
}

userSchema.methods.toJSON = function(){
    try {
        const user = this
        const userObject = user.toObject()
        delete userObject.password
        delete userObject.tokens
        return userObject
    } catch(e){

    }
}

userSchema.statics.findByCredentials = async (identifiant, password) => {
    const user = await User.findOne({$or: [
        {email : identifiant},
        {login : identifiant}

    ]})
    if( !user) throw new Error('unable to login')
    
    const isMatch = await bcrypt.compare(password , user.password)
    if( !isMatch)  throw new Error('unable to login')
    
    return user
}

const User = mongoose.model('User' , userSchema )
module.exports = User
