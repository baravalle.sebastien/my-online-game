const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const validator = require('validator')
const bcrypt = require('bcrypt');

const gameSchema = new mongoose.Schema({
    players: [{
        player:{
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        x:{
            type: Number
        },
        y:{
            type: Number
        },
        direction: {
            type: Number
        },
        pseudo: {
            type: String
        }


    }]
},{
    timestamps:true 
})


gameSchema.pre('save', async function(next ) {
    try {
    /*
        a la creation d'une nouvelle partie cette fonction vide la 
        base de donnée de tout les parties ou:
            1 le player de cette partie se trouve
            2  ou il n'y a pas eu de mise a jour depuis 5 minutes
            3 qui on été créé depuis plus de 3heures
            4 les parties sans players

    */

    const fiveMinutesEarlier =  new Date()
    fiveMinutesEarlier.setMinutes(fiveMinutesEarlier.getMinutes() - 5 )

    const threeHoursEarlier =  new Date()
    threeHoursEarlier.setMinutes(threeHoursEarlier.getMinutes() - 5 )

       const deletedPlayer = await Game.deleteMany( {'$or' : [
           { "players.player": this.players[0].player },
           { updatedAt : { $lt: fiveMinutesEarlier} },
           { createdAt : { $lt: threeHoursEarlier} },
           { createdAt : { $lt: threeHoursEarlier} },
           {'players.0': { "$exists" : false }}

        ]})

        if ( deletedPlayer ){
            console.log("lors de pre save d'une nouvelle partie ");
            console.log("ancienne partie supprimée : ", deletedPlayer);
        }

   } catch( e) {
    console.log(e);
   }

    next()
})

const Game = mongoose.model('Game' , gameSchema )
module.exports = Game
