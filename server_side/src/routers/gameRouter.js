const { request } = require('express')
const express = require('express')
const { ObjectID } = require('mongodb')
const auth= require("../middleware/auth")
const Game = require('../models/game')
const logWriter = require('../utils/logWriter')
const router = new express.Router()
const fs = require('fs')

router.post('/game/create_new_game', auth ,async (req, res )=>{
    const game = new Game({
        players : [{
            pseudo: req.user.login,
            x: 9,
            y: 9,
            direction: 0,
        }]
    })

    try{

        await game.save()
        res.status(200).send(game)


    } catch (e) {
    
        res.status(200).send(

        )
    }
})

router.post('/game/join_game', auth ,async (req, res )=>{

    const xYArray = [[14,4], [12,8],[15, 10],[13,16],[9,15], [8,18], [2,17],[4,9], [7,13],[3,3], [7,4]]
    const randomXY =xYArray[Math.floor(Math.random() * xYArray.length)]

    try{

        // console.log("requete de rejoindre partie : ")
        const playerAlreadyInGame = await Game.findOne({
            'players.player': req.user._id
        })
        if (playerAlreadyInGame ) {
            // console.log(" joueur déja dans partie ");
            // console.log(playerAlreadyInGame);

            res.status(200).send(playerAlreadyInGame)
            return true
        }

        // console.log(" joueur dans aucune partie, ajout joueur a partie en cours ");

        const game = await Game.findOneAndUpdate({
            'players.3': { "$exists" : false }
        }, { $push: {players: {
            pseudo: req.user.login,
            player: req.user._id,
            x: randomXY[0],
            y: randomXY[1],
            direction: 0

        }} } , {new: true}).sort({created_at: -1})

        if(game ) return res.status(200).send(game)
        
        const newGame = new Game({players : [{
            player: req.user._id,
            pseudo: req.user.login,
            x: 9,
            y: 9,
            direction: 0,
        }]})
        await newGame.save()
        res.status(200).send(newGame)


    } catch (e) {
        console.log(e);


        res.status(400).send()
    }
})

/*
    query la game ou un player a id du current player
    renvoie l'element $ pour modification dinamyque
    set sa nouvelle position
*/

// son rôle: est initié en départ de partie, retourne un tableau avec un nouveau joueur ou les joueurs restant lorsque
// certain se connenctent ou se déconnecte, Puis le client re effectue cette requette 

router.post('/game/watch_for_players',async (req, res )=>{

    const changeStream =  Game.watch([
        {$match:
            
            { documentKey:{_id: new ObjectID( req.body.id)}},
            
        }], 
        { fullDocument: 'updateLookup', maxAwaitTimeMS: 1000  }
    )

    const watchNewPlayer = new Promise( resolve=> {
    
    
        const takeLog = []
        let lastLog = ''

        changeStream.on("change",  ( changeEvent)=>{
            const updatedFields = changeEvent.updateDescription.updatedFields
            const updateKey = Object.keys(updatedFields) 
            if(updateKey[0].length === 9  ){
                
                takeLog.push({ i:0, operation: 2, data: updatedFields[updateKey[0]] } )
                
                // console.log(" un joueur s'est connecté  : ",updatedFields);
                changeStream.close()
                resolve({ i:0, operation: 2, data: updatedFields[updateKey[0]] } )
                
            } 
            else if( updateKey[0] === 'players' )
            {
                // console.log(" un joueur s'est déconnecté, joueurs actuels : ",updatedFields.players);
                changeStream.close()
                resolve({i: 0 ,operation: 3, data: updatedFields.players})
                
            }
            
            
        })
    } )
    
    watchNewPlayer.then( data => {
        // console.log("NEWPLAYER   ", data)
        res.status(200).send({data})
    })
              
    
    
    
})




/** **  **  **  **  **  **  **  **  **  **  **  **
 *                                              **
 *      RAPPORTS DE GetLog  ,send moves ,       **
 *      AVEC Log Writer                         **
 *                                              **
 *  **  **  **  **  **  **  **  **  **  **  **  ** 
 *  objectif de ses deux fonctions:
 *      -tenir tout les joueurs au courant 
 *      de la positions des autres joueurs 
 *      sur la map, de la manière la plus économe
 *      possible:
 * 
 *              _____________________________________________________________             ____________________________________________    
 *      --------|   1.  COTÉ CLIENT une demande(x) de position est faite:   |       ------|   3.  COTÉ CLIENT un envoi de position(y) |     
 *      |       |       (fonction recursive )                               |       |     |        (declenché par key event           |
 *      |       |___________________________________________________________|       |     |___________________________________________|
 *      |                                                                ^          |
 *      |                                                                |          |          
 *      |        _____________________________________________           |          |
 *      |-----> |   2.  GET LOG      recoit la demande(x)   |            |          |
 *              |                                           |            |          |       
 *              |   elle WAIT qu'une nouvelle position soit CREE --------|          |              
 *              |    dans le fichier de position du joueur  |                       |
 *              |_____ | ^ _________________________________|                       |
 *                     | |                                                          |
 *                     | |                                            ______________v_______________
 *      (watch changes)| |                                            |  4.  SEND MOVE      (y)     |
 *        __________   | | __       ____________________              |     update la base          |
*        |    FICHIER  v_^  |      |   DATABASE    _    | <-----------                              |
 *       |   dujoueur       |      |___________   | ^___|             |_____________________________|
 *       |_______  ^ _______|                     | |        
 *                 |                              | | (watch changes)   
 *                 |                    _________ | | _________________
 *                 |                    |         |  <  5. LOG WRITER  |
 *                 |                    |         v                    |
 *                 |------------------------ ecrit les postions dans   |
 *                                      |    le fichier dujoueur       |
 *                                      |______________________________|                                       
 * */




router.post('/game/send_moove',async (req, res )=>{


    try{
        const newPosition = await Game.findOneAndUpdate({
            _id : new ObjectID(req.body.id),
               
            players :
                { $elemMatch: {player :req.body.userId}}
        }, 
        { $set: 
            {
                'players.$.x' :req.body.player.x,
                'players.$.y' :req.body.player.y,
                'players.$.direction' :req.body.player.direction
            }
            
        })

        res.status(200).send({})

    } catch (e) {
        console.log(" erreur lors de modification des position ", e );
        res.status(400).send(e)
    }
})

router.post('/game/get_log',async (req, res )=>{
    console.log('get log :  joueur : ', req.body.userId);
    const userId= req.body.userId
    const gameId= req.body.gameId
    const fileName = userId.charAt(2)+gameId.charAt(3)+userId.charAt(4)+gameId.charAt(5)+userId.charAt(6)+gameId.charAt(7)+userId.charAt(8)+gameId.charAt(9)+userId.charAt(1)+gameId.charAt(2)+userId.charAt(3)+gameId.charAt(4)+".json"

    const dataBuffer = await fs.promises.readFile(fileName)
    const dataJSON = dataBuffer.toString()
    const moves = JSON.parse(dataJSON)
    



    if( moves.length > 0 ){
        res.status(200).send(dataJSON)
        fs.writeFileSync(fileName,'[]')

    } else {

        const trigger=  new Promise ( (resolve, reject) => {
            const watcher = fs.watch(fileName,  ()=>{
                const dataBuffer = fs.readFileSync(fileName)
                const dataJSON = dataBuffer.toString()
                watcher.close()
                fs.writeFileSync(fileName,'[]')
        
                return resolve(dataJSON)


            })
        } )

        const result = await trigger
    
        res.status(200).send(result)
        


    }

})




// son role est : pour chaque joueur creer un fichier, et y stocker les logs 

router.post('/game/log_writer', async (req, res )=>{


    const LogWriter = require('../utils/logWriter')
    
    try{
        const logWriter= new LogWriter.logWriter(req.body.userId, req.body.gameId)
        if(req.body.notFirstLogWriter ){
            logWriter.getLastLog()
        } else {
            logWriter.createLog()
        }
        
        const changeStream =   Game.watch([
            {$match:
                
                { documentKey:{_id: new ObjectID( req.body.gameId)}},
                
            }], 
            { fullDocument: 'updateLookup', maxAwaitTimeMS: 0  }
            )
    
            let lastX ; let lastY; let lastDirection 
            let newX ; let newY; let newDirection 
            lastChange=""
        changeStream.on("change",  ( changeEvent)=>{


            // determiner si le fichier de logs a déja été créé
            
            // servent a determiner le caractère de lUpdate > joueur differents de celui qui recoit le logs, 
            //  -> opération de mouvement 
            const updatedFields = changeEvent.updateDescription.updatedFields
            const updateKey = Object.keys(updatedFields) 
            const movingPlayerId = parseInt(updateKey[0].substring(8,9))
            console.log('ID Du Joueur: ',req.body.currentIndex, ' id du joueur qui a bougé ', movingPlayerId )

            if(
                updateKey.length > 0 && updateKey[0].length > 9 &&
                movingPlayerId !==  parseInt(req.body.currentIndex ))
            {


            
            
                const newLog = { data: changeEvent.fullDocument.players[movingPlayerId], index: movingPlayerId }

                
                newX = newLog.data.x; newY = newLog.data.y; newDirection = newLog.data.direction
                
                if ( newX !== lastX || newY !== lastY || newDirection !== lastDirection ){

                    // Aprés tout ce ci qui ne sont que des vérifications, on  peut ajouter le mvt au log 
                    const isError = logWriter.writeLog({x:newX,y:newY ,direction: newDirection,  index: movingPlayerId})

                    if (isError){
                        return true

                    }

                }
                
                lastX = newLog.data.x; lastY = newLog.data.y ; lastDirection = newLog.data.direction
                    
            
                
            }   
        })    
            

    } catch(e){

        console.log(e);
    }

})




router.post('/game/quit_game', auth ,async (req, res )=>{

    try{

        // console.log("update Game deleting one player : ",req.user.login  )

        
        const game = await Game.findOneAndUpdate({
            'players.player': req.user._id
            
        }, { $pull: {
            players : {
                player :  req.user._id  
             }

        } } , {new: true})
        
        // console.log(" new Game  :  ", game) ;

        res.status(200).send({})



    } catch (e) {
        // console.log(e);
        res.status(400).send(e)
    }
})


module.exports = router 

