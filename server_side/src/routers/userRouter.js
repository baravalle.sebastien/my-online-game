const express = require('express')
const auth= require("../middleware/auth")
const User = require('../models/user')
const router = new express.Router()



router.post('/user/register', async  (req, res) => {
    
    const user = new User(req.body)
    try {
        await user.save()
        const token = await user.generateAuthToken()
        user.token = token
        res.status(201).send({user, token })
        
    } catch ( e ) {
        
        res.status(400).send(e)
        console.log(e);
    }
})


router.post('/user/login', async (req, res ) => {
    try{
        const user = await User.findByCredentials(req.body.identifiant, req.body.password)
        const token = await user.generateAuthToken()
        res.send({ user, token})

    } catch(e) {
        res.status(400).send(e)
        console.log(e);

    }
})

router.get('/user/me', auth,async (req, res) => {

    res.status(200).send(req.user)    
})

router.post('/user/log_out',auth,  async (req, res ) => {
    try{
        req.user.tokens = []
        const hookPreSummary = 1
        const resultat = await req.user.save(hookPreSummary)
        res.send({})

    } catch(e) {
        res.status(400).send(e)
        console.log(e);

    }
})

module.exports = router 