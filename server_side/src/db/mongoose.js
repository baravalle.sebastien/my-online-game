const mongoose = require('mongoose')
const connectionURL = 'mongodb://localhost:27017,localhost:27018,localhost:27019/task-manager-api?replicaSet=rs0'
mongoose.connect(connectionURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify:false

})
