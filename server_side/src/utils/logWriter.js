const fs = require('fs')

const logWriter= class LogWriter{

    // sameGame : le log est lancé par une requette, celle ci va finir par s'arreter, 
    // alors il faudra reprend la partie en cours 
    // par contre si c'est le premier il faut écraser le log déja écrit
    constructor(userId, gameId){
        this.fileName = userId.charAt(2)+gameId.charAt(3)+userId.charAt(4)+gameId.charAt(5)+userId.charAt(6)+gameId.charAt(7)+userId.charAt(8)+gameId.charAt(9)+userId.charAt(1)+gameId.charAt(2)+userId.charAt(3)+gameId.charAt(4)+".json"
        this.lastLog = []
        console.log("initié ");

    }


    createLog(){
        fs.writeFileSync(this.fileName, '[]')

    }

    getLastLog(){
        const dataBuffer = fs.readFileSync(this.fileName)
        try{

            const dataJSON = dataBuffer.toString()
            this.lastLog = JSON.parse(dataJSON)
        } catch(e){
            this.lastLog= []
        }

    }


    writeLog(log){
        this.getLastLog()

        if( this.lastLog.length > 0 ){

            console.log(' same position in log');

            // PCP le watcher a été dupliqué par un reload il faut l'arreter
            console.log(
                'last postion ',
                this.lastLog[this.lastLog.length-1],
                'new position',
                log
            )
            if ( 
                parseInt(this.lastLog[this.lastLog.length-1].x) === parseInt(log.x) &&
                parseInt(this.lastLog[this.lastLog.length-1].y) === parseInt(log.y) &&
                parseInt(this.lastLog[this.lastLog.length-1].direction) === parseInt(log.direction) &&
                parseInt(this.lastLog[this.lastLog.length-1].index) === parseInt(log.index)


                
            ){
                    console.log(" il y a un même log writer ");            
                    return true

            }
            

        }
        this.lastLog.push(log)
        const dataJSON = JSON.stringify(this.lastLog)

        fs.writeFileSync(this.fileName, dataJSON)
        
    }

}

module.exports = {logWriter}
