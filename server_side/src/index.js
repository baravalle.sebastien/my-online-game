const express = require('express')
const app = express()
require('./db/mongoose')

var cors = require('cors')
// var corsOptions = {
//     origin: ['http://192.168.1.31:9000', ''],
//     optionsSuccessStatus: 200 
// }


app.use(cors())


app.use(express.json())

const userRouter = require('./routers/userRouter')
app.use(userRouter)

const gameRouter = require('./routers/gameRouter')
app.use(gameRouter)



const port = 3000

app.listen(port, () => {
    console.log("Server is up on port "+port);
})








const { MongoClient, ObjectID } = require("mongodb")
const uri = "mongodb://localhost:27017,localhost:27018,localhost:27019?replicaSet=rs0"

